var app = {
    init: function () {
        app.customSelect();
        app.maximumSelection();
        app.formValidation();
        app.dataTable();
        app.datePicker();
        app.timePicker();

        app.ajaxRoute = $('meta[name="ajax-route"]').attr('content');
    },

    ajaxRoute : null,

    datePicker: function(){
        $(".date-picker").each(function(){
            var options = {
                format : $(this).data('format'),
                selectYears: !0,
                selectMonths: !0
            };
            $(this).pickadate(options);
        })

    },
    timePicker: function(){
        $('.pickatime').each(function(){
            var options = {
                formatLabel: 'HH:i a',
                formatSubmit: 'HH:i',
            };
            $(this).pickatime(options);
        })

    },

    dataTable: function(){
        $('.zero-configuration').DataTable();
    },
    
    formValidation : function(){
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    },

    customSelect:function (){
        if($(".select2").length){
            $(".select2").select2({
                dropdownAutoWidth: true,
                width: '100%'
            });
        }
    },

    maximumSelection : function(){
        $(".max-length").each(function(){
            var max = $(this).data('max');
            $(this).select2({
                dropdownAutoWidth: true,
                width: '100%',
                maximumSelectionLength: max,
                placeholder: "Select maximum "+ max +" items"
            });
        })
    },
    disableButton: function(el){
        $(el).attr('disabled' , true);
        $(el).prepend('<span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>');
    },
    enableButton: function(el){
        $(el).removeAttr('disabled');
        $(el).find('.spinner-border').hide();
    },
    activateRetriveBtn : function(){
        var address = $('[name="address"]').val();
        var area = $('[name="area"]').val();
        if(address && area){
            $('#retrive-coordinates').removeAttr('disabled');
        }
    },
    checkFamilyProperty : function(el){
        var val = $(el).val();
        if(val.length > 8){

            var prop = $(el).data('prop');

            var data = {
                prop : prop,
                value : val,
                action: 'families.checkProprty'
            }

            app.appAjax('POST' , data , app.ajaxRoute , 'object')
            .then(function(response){
                if(response.status == 'ok'){
                   
                    $(el).closest('.form-group').addClass('error');
                    $(el).next('.help-block').show().html('<ul role="alert"><li>This is used for another <a href="'+response.url+'"><strong>family!</strong></a></li></ul>');
                    
                }
            })
        }
    },
    retriveCoordinates: function(element){
        event.preventDefault();
        app.disableButton(element);
        
        var address = $('[name="address"]').val();
        var area = $('[name="area"]').val();
        var city = $('[name="city"]').val();
        var emirate = $('[name="emirate"]').val();
        
        if(address && area){
            var data = {
                address : address,
                area : area,
                city : city,
                emirate : emirate,
                action : 'families.retriveCoordinates'
            }
            app.appAjax('POST' , data , app.ajaxRoute , 'object')
                .then(function(response){
                    if(response.status == 'ok'){                        
                        $('[name="lat"]').val(response.coordinates.lat);                   
                        $('[name="long"]').val(response.coordinates.long);
                        app.enableButton(element);                   

                    }
                })
                .fail(function(jqXHR, textStatus, error){
                    if(error == 'Not Found'){
                        
                        toastr.error('Change Address and Area, Or add the cordinates manually', 'No Results Founded!');

                    }else{
                        toastr.error('Please try again later!','Something went wrong');
                    }

                    app.enableButton(element);                   

                });
            }else{
                app.enableButton(element);                   
                toastr.error('Please add the address and the area!','Something went wrong');
            }
        
    },
    /** Delete Entity from DB */
    deleteItem : function(el){
        var route = $(el).data('route');

        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!",
            confirmButtonClass: "btn btn-outline-success",
            cancelButtonClass: "btn btn-outline-danger ml-1",
            buttonsStyling: !1
        }).then(function(t) {
            if(t.value){
                app.appAjax('DELETE' , {} ,route , 'object')
                .then(function(response){
                    if(response.status == 'ok'){                        
                        toastr.success(response.message , "Deleted Successfully!" );
                        $('#' + response.deleted ).fadeOut();
                    }
                })
                .fail(function(jqXHR, textStatus, error){
                    toastr.error('Please try again later!','Something went wrong');
                });
            }
        })
        
    },
    toggleElement : function(el){
        var val         = $(el).val();
        var show        = $(el).data('show');
        var condition   = ($(el).data('condition')).split("|");

            switch (condition[0]) {
                case "eq":
                    app.toggleElementIfEqual(val , show , condition[1])
                    break;
            }
    },

    toggleElementIfEqual: function(val , show , condition){
        if(val == condition){
            $("#" + show).fadeIn();
        }else{
            $("#" + show).fadeOut();
        }      
    },

    appAjax : function(method , data , route , dataType ){
        // Default DataType is Data
        dataType = dataType || 'data';
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
    
        var args = {
              url           : route,
              method        : method,
              data          : data,
              dataType      : "json",
           };
    
    
        // For Ajax File Upload
        if(dataType == 'file'){
           args.processData = false; 
           args.contentType = false; 
        }
        return $.ajax(args);
    }
}

$(document).ready(function () {
    app.init();
});